﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using log4net;
using log4net.Config;

namespace AspNetMvcAndLog4Net
{
    public class MvcApplication : HttpApplication
    {
        private ILog log = LogManager.GetLogger("Web");

        protected void Application_Start()
        {

            XmlConfigurator.Configure();//start log4net

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // Get the exception object.
            Exception ex = Server.GetLastError();

            // Handle HTTP errors

            //log error to log file
            log.Error(ex);

            // Clear the error from the server
            Server.ClearError();
        }

    }
}