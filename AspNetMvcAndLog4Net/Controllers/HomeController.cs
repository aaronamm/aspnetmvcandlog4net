﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace AspNetMvcAndLog4Net.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        private ILog _log = LogManager.GetLogger("AspNetMvcAndLog4Net");

        public ActionResult Index()
        {
            _log.Debug("hit /Home/Index");
            return Content("Home/Index");
        }

        public ActionResult Error()
        {
            throw new Exception("unhandle exception");
        }

        public ActionResult Fatal()
        {
            var ex = new  Exception("Fatal error need hot fix");
            _log.Fatal(ex);
            return Content("fatal");
        }
    }
}
